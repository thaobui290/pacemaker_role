Role Name
=========

Pacemaker role

Requirements
------------

This role is written for Ubuntu 14.04

Role Variables
--------------

Role Variables are locate in:

* defaults/main.yml: default variables for set up

>
>logpath: path to the directory you want to store log file
>
>config_path: /etc/corosync/corosync.conf
>
>mcast_port: your multicast port for corosync
>
>cluster_name: your cluster name
>
>coro_service: corosync
>
>pcmk_service: pacemaker
>
>pkg:
>
> - pacemaker 

Dependencies
------------

No Dependencies

Example Playbook
----------------

An example of how to use this role (for instance, with variables passed in as parameters):

Use custom inventory: 
>hosts: all
>   
>remote_user: vagrant
> 
>sudo: yes
>
>roles:
>
- pacemaker


License
-------

BSD

Author Information
------------------

Mr. BinhND

Email: binhnd.ask@gmail.com
